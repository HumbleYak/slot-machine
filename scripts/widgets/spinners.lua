local Storyboard = require("sbutils/storyboard")

---
--@module spinners
--@type spinners
local Spinners = {}

local images1 = {
                  "blue",
                  "green",
                  "red",
                  "purple",
                  "yellow",
                  "white",
               }
               
local images2 = {
                  "green",
                  "white",
                  "purple",
                  "red",
                  "yellow",
                  "blue",
               }
               
local images3 = {
                  "purple",
                  "yellow",
                  "blue",
                  "white",
                  "green",
                  "red",
               }


local table1 = Storyboard.wrap_table("spinner1", "spinner_layer")
local table2 = Storyboard.wrap_table("spinner2", "spinner_layer")
local table3 = Storyboard.wrap_table("spinner3", "spinner_layer")
table1.images =  images1
table2.images = images2
table3.images = images3
local win = Storyboard.wrap_control("win", "top_bg_layer")
local go = Storyboard.wrap_control("go","top_bg_layer")

math.randomseed(os.time())

---
--@return #spinners
function Spinners.init_spinners()
  local new_spinners = {}
  setmetatable(new_spinners, {__index=Spinners})

  gre.load_image("images/cell_bg_blur.png")
  for i=1, #images1 do
    local image = string.format("images/%s.png", images1[i])
    gre.load_image(image)
    
    image = string.format("images/%s_blur.png", images1[i])
    gre.load_image(image)
  end
  
  local i = 0
  for row = 1, table1:get_rows() do
    i = i + 1
    if(i > #images1) then
      i = 1
    end
    
    local image1 = string.format("images/%s.png", images1[i])
    local image2 = string.format("images/%s.png", images2[i])
    local image3 = string.format("images/%s.png", images3[i])
    table1:set_cell_value(row, 1, "image", image1)
    table2:set_cell_value(row, 1, "image", image2)
    table3:set_cell_value(row, 1, "image", image3)    
  end
  
  new_spinners.cell_height = table1:get_cell_height()
  
  return new_spinners
end

local function addAnimationStep(animation, key, from, to, offset, duration)
  local data = {}
  
--  print("addAnimationStep", animation, key, from, to, offset, duration)
  
  data.key = key
  data.from = from
  data.to  = to
  data.offset = offset
  data.duration = duration
  data.rate = "linear"
  
  gre.animation_add_step(animation, data)
end

local function spinTable(t, animation, spins)
  local rows = t:get_rows()
  local cell_height = t:get_cell_height()
  local max_offset = -(rows-2)*cell_height
  
  --Change all the icons to the blurry version
  local i = 0
  for row=1, rows do
    i = i + 1
    if(i > #images1) then
      i = 1
    end
    
    local image1 = string.format("images/%s_blur.png", t.images[i])
    local image2 = "images/cell_bg_blur.png"
    addAnimationStep(animation, t:cell_key(row, 1, "image"), nil, image1, 0, 0)
    addAnimationStep(animation, t:cell_key(row, 1, "bg"), nil, image2, 0, 0)
  end
  
  local step_duration = 500
  
  for i=1, spins do
    addAnimationStep(animation, t:key("grd_yoffset"), 0, max_offset, (i-1)*step_duration, step_duration)
  end

  local yoff = math.random(0, (rows-2) * (step_duration*2) - 1)
  yoff = (yoff % (rows-2)) * - (cell_height)
  local duration = (step_duration*0.8)+((step_duration*0.2)*yoff/max_offset)
  addAnimationStep(animation, t:key("grd_yoffset"), 0, yoff, spins*step_duration, step_duration)  
  
    local offset = (spins+1)*step_duration - (step_duration*0.4)
  --restore all the icons to the clear version
  i = 0
  for row=1, rows do
    i = i + 1
    if(i > #images1) then
      i = 1
    end
    
    local image1 = string.format("images/%s.png", t.images[i])
    local image2 = "images/cell_bg.png"
    addAnimationStep(animation, t:cell_key(row, 1, "image"), nil, image1, offset, 0)
    addAnimationStep(animation, t:cell_key(row, 1, "bg"), nil, image2, offset, 0)
  end
end

function Spinners:win()
  gre.timer_set_timeout(function()
                          win:set_hidden(false)
                          gre.timer_set_timeout(function ()
                                                  win:set_hidden(true)
                                                end, 5000)
                        end, 500)
end

function Spinners:spun()
  local yoff1 = table1:get_yoffset()
  local yoff2 = table2:get_yoffset()
  local yoff3 = table3:get_yoffset()

  local cell_height = self.cell_height
  local row_1_1 = ((yoff1 / -cell_height) % #images1) + 1
  local row_2_1 = ((yoff1 / -cell_height + 1) % #images1) + 1
  local row_3_1 = ((yoff1 / -cell_height + 2) % #images1) + 1
  local row_1_2 = ((yoff2 / -cell_height) % #images2) + 1
  local row_2_2 = ((yoff2 / -cell_height + 1) % #images2) + 1
  local row_3_2 = ((yoff2 / -cell_height + 2) % #images2) + 1
  local row_1_3 = ((yoff3 / -cell_height) % #images3) + 1
  local row_2_3 = ((yoff3 / -cell_height + 1) % #images3) + 1
  local row_3_3 = ((yoff3 / -cell_height + 2) % #images3) + 1

  local cell_1_1 = table1:get_cell_value(row_1_1, 1, "image")
  local cell_2_1 = table1:get_cell_value(row_2_1, 1, "image")
  local cell_3_1 = table1:get_cell_value(row_3_1, 1, "image")
  local cell_1_2 = table2:get_cell_value(row_1_2, 1, "image")
  local cell_2_2 = table2:get_cell_value(row_2_2, 1, "image")
  local cell_3_2 = table2:get_cell_value(row_3_2, 1, "image")
  local cell_1_3 = table3:get_cell_value(row_1_3, 1, "image")
  local cell_2_3 = table3:get_cell_value(row_2_3, 1, "image")
  local cell_3_3 = table3:get_cell_value(row_3_3, 1, "image")
  
  if(
      (cell_1_1 == cell_1_2 and cell_1_1 == cell_1_3) or
      (cell_2_1 == cell_2_2 and cell_2_1 == cell_2_3) or
      (cell_3_1 == cell_3_2 and cell_3_1 == cell_3_3) or
      (cell_1_1 == cell_2_2 and cell_1_1 == cell_3_3) or
      (cell_3_1 == cell_2_2 and cell_3_1 == cell_1_3))
  then
    self:win()
  end
  
--  print(cell_2_1, cell_2_2, cell_2_3)
end

function Spinners:spin()
  local animation = gre.animation_create(60, true, function()
    self:spun()
  end)
  spinTable(table1, animation, 2)
  spinTable(table2, animation, 4)
  spinTable(table3, animation, 7)
  gre.animation_trigger(animation)
end

return Spinners