local Spinners = require("widgets/spinners")

---Rotated table cells are not being clipped by the table bounds.  (Not a regression from 5.3)
--
--Disappearing Cell if 3D rotation points to a variable that wasn't created at runtime.  (Not a regression from 5.3)
--
--Global lua functions that are referenced earlier in the script
--than they are declared will not show up in the Outline
--Fix for this is to use @function [parent=#global] functionname
--
--Failed to open table cell variable creation dialog
--
--Scrolling does not snap if we don't flick
--
--Can't rotate a table cell with multiple render extensions
--
--Image corruption on the Simpsons GIF (does not happen in google)
--
--One of the coin gifs doesn't animate correctly

local spinners

--- @param gre#context mapargs
function cb_init(mapargs)
  spinners = Spinners.init_spinners()
end

--- @param gre#context mapargs
function cb_go(mapargs) 
  spinners:spin()
end
